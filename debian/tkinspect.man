[comment {-*- tcl -*- doctools manpage}]
[manpage_begin tkinspect 1 5.1.6p10]
[moddesc   {Tkinspect}]
[titledesc {Tk application inspector}]

[description]
[para]
[cmd tkinspect] is an inspector for Tk applications. It uses Tk's
[cmd send] command to retreive infomation from other Tk applications.

[list_begin definitions]
[call [cmd tkinspect]]
[list_end]

[para]
When you choose an application through the [arg {File/Select Interpreter}]
menu, lists will be filled with the names of the procs, globals, and
windows the the application. Clicking on one of those names will fill
the value window with the definition of the proc, the value of the
global variable, or various information about the window. The value
window is editable and its contents can be sent back to the selected
application.

[para]
Tcl commands can be sent to the seleted application using the [arg Command:]
entry, or through a command line (via the [arg {File/New Command Line}] menu)
interface to the application. The command line is nearly identical
to Tk's demo [cmd rmt].

[para]
For further information, select a topic from the help window's [arg Topics]
menu.

[see_also send(3tk) /usr/share/doc/tk8.5/examples/rmt]
[keywords Tcl Tk]
[manpage_end]
